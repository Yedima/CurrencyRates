package com.example.currencyrates.clientservice.model;

import com.google.gson.annotations.SerializedName;
import lombok.*;
import javax.persistence.Embedded;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ClientExchangeRequest {

    @SerializedName("certainDate")
    private String certainDate;

    @Embedded
    @SerializedName("foreignCurrencies")
    private ForeignCurrency[] foreignCurrencies;

}
