package com.example.currencyrates.clientservice.service;

import com.example.currencyrates.bankservice.model.CurrencyUnit;
import com.example.currencyrates.clientservice.model.ForeignCurrency;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CurrencyValidator {

    private static final List<String> ALL_POSSIBLE_CURRENCIES = List.of("USD", "AUD", "CAD", "EUR", "HUF", "CHF", "GBP", "JPY", "CZK", "DKK", "NOK","SEK", "XDR");

    public boolean validateCurrencies(List<ForeignCurrency> allForeignCurrenciesToValidate) {
        return ALL_POSSIBLE_CURRENCIES.containsAll(allForeignCurrenciesToValidate.stream()
                .map(ForeignCurrency::getCurrencyCode)
                .collect(Collectors.toList()));
    }

}
