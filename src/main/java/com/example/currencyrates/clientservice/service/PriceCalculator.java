package com.example.currencyrates.clientservice.service;

import com.example.currencyrates.bankservice.model.CurrencyUnit;
import com.example.currencyrates.clientservice.model.ForeignCurrency;
import org.springframework.stereotype.Component;
import java.math.BigDecimal;
import java.util.List;

@Component
public class PriceCalculator {

    public BigDecimal calculateTotalPrice(List<ForeignCurrency> currenciesToCalculate, List<CurrencyUnit> bankExchangeRate) {
        BigDecimal big = currenciesToCalculate.stream()
                .map(currency -> bankExchangeRate.stream()
                        .filter(rate -> rate.getCode().equals(currency.getCurrencyCode()))
                        .findFirst()
                        .orElseThrow().getAsk().multiply(currency.getCurrencyAmount()))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(big);
        return big;
    }

}
