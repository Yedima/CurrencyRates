package com.example.currencyrates.clientservice.controller;

import com.example.currencyrates.bankservice.model.CurrencyDay;
import com.example.currencyrates.clientservice.model.ClientExchangeRequest;
import com.example.currencyrates.bankservice.model.CurrencyUnit;
import com.example.currencyrates.clientservice.model.ForeignCurrency;
import com.example.currencyrates.clientservice.service.CurrencyDayService;
import com.example.currencyrates.clientservice.service.CurrencyUnitService;
import com.example.currencyrates.clientservice.service.CurrencyValidator;
import com.example.currencyrates.clientservice.service.PriceCalculator;
import com.google.gson.Gson;
import jdk.swing.interop.SwingInterOpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/currencies")
public class ClientRequestController {

    private final PriceCalculator priceCalculator;
    private final CurrencyValidator currencyValidator;
    private final CurrencyDayService currencyDayService;
    private final CurrencyUnitService currencyUnitService;

    @Autowired
    public ClientRequestController(PriceCalculator priceCalculator, CurrencyValidator currencyValidator, CurrencyDayService currencyDayService, CurrencyUnitService currencyUnitService) {
        this.priceCalculator = priceCalculator;
        this.currencyValidator = currencyValidator;
        this.currencyDayService = currencyDayService;
        this.currencyUnitService = currencyUnitService;
    }

    @PostMapping("/priceOfCurrencies")
    public BigDecimal getThePriceOfCertainCurrenciesAmount(@RequestBody ClientExchangeRequest clientExchangeRequest) {

        if (!this.currencyValidator.validateCurrencies(List.of(clientExchangeRequest.getForeignCurrencies()))) {
            throw new HttpStatusCodeException(HttpStatus.BAD_REQUEST, "One currency from your request isn't supported") {
                @Override
                public HttpStatus getStatusCode() {
                    return super.getStatusCode();
                }
            };
        }


        //TODO: HANDLE THIS METHOD AND CLEAN UP ALL THE MESS HERE
        //TODO REFACTORING OF MODELS BETWEEN SERVICES


        List<CurrencyUnit> allCurrencyUnitsForCertainDay;
        List<ForeignCurrency> allForeignCurrencyFromRequest;

        Optional<CurrencyDay> currencyDay = checkIfDatabaseAlreadyHasTable(clientExchangeRequest.getCertainDate());

        if(currencyDay.isPresent()) {

            allCurrencyUnitsForCertainDay = this.currencyUnitService.getCurrencyUnitsByCertainDayId(currencyDay.get().getId());
            allForeignCurrencyFromRequest = Arrays.stream(clientExchangeRequest.getForeignCurrencies())
                    .collect(Collectors.toList());

        } else {
            try {
                HttpRequest getRequest = HttpRequest.newBuilder()
                        .uri(new URI("http://localhost:8080/api/currencies/currencyRatesCertainDate/" + clientExchangeRequest.getCertainDate()))
                        .header("Accept", "application/json")
                        .GET()
                        .build();

                HttpClient httpClient = HttpClient.newHttpClient();
                HttpResponse<String> getResponse = httpClient.send(getRequest, HttpResponse.BodyHandlers.ofString());

                Gson gson = new Gson();

                                            allCurrencyUnitsForCertainDay = Arrays.stream(gson.fromJson(getResponse.body(), CurrencyUnit[].class))
                                                    .collect(Collectors.toList());
                                            allForeignCurrencyFromRequest = Arrays.stream(clientExchangeRequest.getForeignCurrencies())
                                                    .collect(Collectors.toList());

                CurrencyDay[] currencyDayByBankAPI = gson.fromJson(getResponse.body(), CurrencyDay[].class);
                System.out.println(currencyDayByBankAPI[0]);


                if (currencyDayByBankAPI != null) {
                    this.currencyDayService.saveCurrencyDay(currencyDayByBankAPI[0]);
                    this.currencyUnitService.saveCurrencyUnits(new HashSet<>(allCurrencyUnitsForCertainDay), currencyDayByBankAPI[0]);
                }


            } catch (URISyntaxException | IOException | InterruptedException exception) {
                System.err.println("Something went wrong in getThePriceOfCertainCurrenciesAmount method");
                return new BigDecimal("0.0");
            }


        }
        System.out.println(allForeignCurrencyFromRequest);
        System.out.println(allCurrencyUnitsForCertainDay);
        return this.priceCalculator.calculateTotalPrice(allForeignCurrencyFromRequest, allCurrencyUnitsForCertainDay);
    }


    private Optional<CurrencyDay> checkIfDatabaseAlreadyHasTable(String requiredDate) {
        return this.currencyDayService.getTableByDate(requiredDate);
    }

}
